#docker login --username $DOCKER_USERNAME --password $DOCKER_PASSWORD
# Build image
docker build -t blesafho/micro:jobjs .

# Push container
docker push blesafho/micro:jobjs

# Create container
#docker container create --name jobjs blesafho/micro:jobjs

# Start container
#docker container start nodejs-job

# See container logs
#docker container logs -f nodejs-job

# Stop container
#docker container stop nodejs-job

# Remove container
#docker container rm nodejs-job
